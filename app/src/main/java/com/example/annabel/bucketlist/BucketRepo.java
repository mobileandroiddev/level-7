package com.example.annabel.bucketlist;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class BucketRepo{

    private AppDatabase appDatabase;
    private BucketDao bucketDao;
    private LiveData<List<BucketItem>> buckets;
    private Executor executor = Executors.newSingleThreadExecutor();

    public BucketRepo (Context context) {
        appDatabase = AppDatabase.getInstance(context);
        bucketDao = appDatabase.bucketDao();
        buckets = bucketDao.getAllReminders();
    }

    public LiveData<List<BucketItem>> getAllReminders() {
        return buckets;
    }

    public void insert(final BucketItem bucketItem) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                bucketDao.insertBuckets(bucketItem);
            }
        });
    }

    public void update(final BucketItem bucketItem) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                bucketDao.updateBuckets(bucketItem);
            }
        });
    }

    public void delete(final BucketItem bucketItem) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                bucketDao.deleteBuckets(bucketItem);
            }
        });
    }
}