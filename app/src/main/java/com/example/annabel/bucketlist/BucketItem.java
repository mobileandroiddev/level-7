package com.example.annabel.bucketlist;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity (tableName = "bucket")
public class BucketItem implements Parcelable {

    @PrimaryKey (autoGenerate = true)
    private Long id;
    @ColumnInfo (name = "bucketTitle")
    private String bucketTitle;
    @ColumnInfo (name = "bucketDescription")
    private String bucketDescription;
    @ColumnInfo (name = "bucketChecked")
    private Boolean bucketChecked;

    public BucketItem(String bucketTitle, String bucketDescription) {
        this.bucketTitle = bucketTitle;
        this.bucketDescription = bucketDescription;
        this.bucketChecked = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    @Override
//    public String toString() {
//        return mReminderText;
//    }

    public String getBucketTitle() {
        return bucketTitle;
    }

    public void setBucketTitle(String bucketTitle) {
        this.bucketTitle = bucketTitle;
    }

    public String getBucketDescription() {
        return bucketDescription;
    }

    public void setBucketDescription(String bucketDescription) {
        this.bucketDescription = bucketDescription;
    }

    public Boolean getBucketChecked() {
        return bucketChecked;
    }

    public void setBucketChecked(Boolean bucketChecked) {
        this.bucketChecked = bucketChecked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.bucketTitle);
        dest.writeString(this.bucketDescription);
        dest.writeValue(this.bucketChecked);
        dest.writeValue(this.id);
    }

    protected BucketItem(Parcel in) {
        this.bucketTitle = in.readString();
        this.bucketDescription = in.readString();
        this.bucketChecked = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.id = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Creator<BucketItem> CREATOR = new Creator<BucketItem>() {
        @Override
        public BucketItem createFromParcel(Parcel source) {
            return new BucketItem(source);
        }

        @Override
        public BucketItem[] newArray(int size) {
            return new BucketItem[size];
        }
    };
}
