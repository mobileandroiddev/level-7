package com.example.annabel.bucketlist;

import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

public class BucketItemAdapter extends RecyclerView.Adapter<BucketItemAdapter.ViewHolder> {

    private List<BucketItem> bucketList;
    final private BucketClickListener bucketClickListener;

    public BucketItemAdapter(List<BucketItem> bucketList, BucketClickListener bucketClickListener) {
        this.bucketList = bucketList;
        this.bucketClickListener = bucketClickListener;
    }

    public interface BucketClickListener{
        void bucketOnClick (int i);
    }

    @NonNull
    @Override
    public BucketItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bucket_view, viewGroup, false);
        BucketItemAdapter.ViewHolder viewHolder = new BucketItemAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BucketItemAdapter.ViewHolder viewHolder, int i) {
        BucketItem bucketItem =  bucketList.get(i);
        viewHolder.title.setText(bucketItem.getBucketTitle());
        viewHolder.description.setText(bucketItem.getBucketDescription());
        checkBoxHandler(viewHolder);

        if (bucketItem.getBucketChecked()){
            viewHolder.title.setPaintFlags(viewHolder.title.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            viewHolder.description.setPaintFlags(viewHolder.description.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            viewHolder.checkBox.setChecked(true);
        } else {
            viewHolder.title.setPaintFlags(viewHolder.title.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            viewHolder.description.setPaintFlags(viewHolder.description.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            viewHolder.checkBox.setChecked(false);
        }
    }

    @Override
    public int getItemCount() { return bucketList.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title;
        public TextView description;
        public CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.titleView);
            description = itemView.findViewById(R.id.descriptionView);
            checkBox = itemView.findViewById(R.id.checkBox);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            bucketClickListener.bucketOnClick(clickedPosition);
        }
    }

    private void checkBoxHandler(final BucketItemAdapter.ViewHolder viewHolder){
        viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean checked = ((CheckBox) view).isChecked();
                BucketItem item = bucketList.get(viewHolder.getAdapterPosition());
                if (checked){
                    Log.i("KLIK", "CHECKED");
                    item.setBucketChecked(true);
                } else {
                    Log.i("KLIK", "NOT CHECKED");
                    item.setBucketChecked(false);
                }
                MainActivity.getInstance().mainViewModel.update(item);
                updateList(bucketList);
            }
        });
    }

    public void updateList (List<BucketItem> newList) {
        bucketList = newList;
        if (newList != null) {
            this.notifyDataSetChanged();
        }
    }
}