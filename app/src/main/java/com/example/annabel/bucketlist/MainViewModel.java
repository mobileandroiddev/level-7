package com.example.annabel.bucketlist;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import java.util.List;

public class MainViewModel extends ViewModel {

    private BucketRepo repository;
    private LiveData<List<BucketItem>> buckets;

    public MainViewModel(Context context) {
        repository = new BucketRepo(context);
        buckets = repository.getAllReminders();
    }

    public LiveData<List<BucketItem>> getBuckets() {
        return buckets;
    }

    public void insert(BucketItem bucket) {
        repository.insert(bucket);
    }

    public void update(BucketItem bucket) {
        repository.update(bucket);
    }

    public void delete(BucketItem bucket) {
        repository.delete(bucket);
    }
}
